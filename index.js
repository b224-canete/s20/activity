// console.log("Hello World");

let getNumber = Number(prompt("Give me a number: "));
console.log("The number you provided is " + getNumber + ".");

for( getNumber ; getNumber >=0; getNumber--) {
	
	if (getNumber <= 50) {
		console.log("The current value is at 50. Terminating the loop.")
		break;
	};

	if(getNumber % 10 === 0){
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	};

	if(getNumber % 5 === 0){
		console.log(getNumber);
		continue;
	};

	
};

console.log("");

let fullWord = "supercalifragilisticexpialidocious";
let consoWord = "";

for(let y = 0; y < fullWord.length; y++) {
	if (
		fullWord[y].toLowerCase() == "a" ||
		fullWord[y].toLowerCase() == "e" ||
		fullWord[y].toLowerCase() == "i" ||
		fullWord[y].toLowerCase() == "o" ||
		fullWord[y].toLowerCase() == "u" 
	) {
		continue;
	} else {
		consoWord += fullWord [y];
	};
};

console.log(fullWord);
console.log(consoWord);